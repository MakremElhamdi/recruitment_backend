from flask import Flask

from flask_pymongo import PyMongo

from bson.json_util import dumps

from bson.objectid import ObjectId

from flask import jsonify, request
from models import OffreEmploi
from datetime import datetime


from werkzeug.security import generate_password_hash, check_password_hash
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity



app = Flask(__name__)

app.config['JWT_SECRET_KEY'] = 'your_secret_key'
jwt = JWTManager(app)

app.config['MONGO_URI'] = "mongodb://localhost:27017/Users"

mongo = PyMongo(app)

@app.route('/users')
def get_all_users():
    candidates = mongo.db.candidates.find()
    recruiters = mongo.db.recruiters.find()
    all_users = list(candidates) + list(recruiters)
    resp = dumps(all_users)
    return resp

@app.route('/users/<user_type>')
def get_all_users_by_type(user_type):
    valid_user_types = ['candidat', 'recruiter', 'admin']
    users = []

    if user_type.lower() not in valid_user_types:
        abort(400, {'error': 'Invalid user type'})

    if user_type == 'admin':
        # Admin can see all users, regardless of type
        users = list(mongo.db.candidates.find()) + list(mongo.db.recruiters.find())
    elif user_type == 'candidat':
        users = list(mongo.db.candidates.find())
    elif user_type == 'recruiter':
        users = list(mongo.db.recruiters.find())

    resp = dumps(users)
    return resp

@app.route('/candidate/<id>')
def get_candidate_by_id(id):
    candidate = mongo.db.candidates.find_one({'_id': ObjectId(id)})
    resp = dumps(candidate)
    return resp

@app.route('/recruiter/<id>')
def get_recruiter_by_id(id):
    recruiter = mongo.db.recruiters.find_one({'_id': ObjectId(id)})
    resp = dumps(recruiter)
    return resp

@app.route('/candidates')
def get_all_candidates():
    candidates = mongo.db.candidates.find()
    resp = dumps(candidates)
    return resp

@app.route('/recruiters')
def get_all_recruiters():
    recruiters = mongo.db.recruiters.find()
    resp = dumps(recruiters)
    return resp

@app.route('/candidate/<id>', methods=['DELETE'])
def delete_candidate(id):
    mongo.db.candidates.delete_one({'_id': ObjectId(id)})
    resp = jsonify("Candidate deleted successfully")
    resp.status_code = 200
    return resp

@app.route('/recruiter/<id>', methods=['DELETE'])
def delete_recruiter(id):
    mongo.db.recruiters.delete_one({'_id': ObjectId(id)})
    resp = jsonify("Recruiter deleted successfully")
    resp.status_code = 200
    return resp


@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message':'Not Found' + request.url
    }
    resp = jsonify(message)

    resp.status_code = 404

    return  resp

@app.route('/register/candidate', methods=['POST'])
def register_candidate():
    _json = request.json
    _firstName = _json['firstName']
    _lastName = _json['lastName']
    _phoneNumber = _json['phoneNumber']
    _emailAddress = _json['emailAddress']
    _password = _json['password']
    _type = _json['type']
    _title = _json['title']
    _resume_path = _json['resume_path']

    _hashed_pwd = generate_password_hash(_password)

    id = mongo.db.candidates.insert_one({
        'firstName': _firstName,
        'lastName': _lastName,
        'phoneNumber': _phoneNumber,
        'emailAddress': _emailAddress,
        'password': _hashed_pwd,
        'type': _type,
        'title': _title,
        'resume_path': _resume_path
    })
    resp = jsonify({'register': True, 'user_type': _type})
    resp.status_code = 200
    return resp

@app.route('/register/recruiter', methods=['POST'])
def register_recruiter():
    _json = request.json
    _firstName = _json['firstName']
    _lastName = _json['lastName']
    _phoneNumber = _json['phoneNumber']
    _emailAddress = _json['emailAddress']
    _password = _json['password']
    _type = _json['type']
    _name = _json['name']
    _address = _json['address']

    _hashed_pwd = generate_password_hash(_password)

    id = mongo.db.recruiters.insert_one({
        'firstName': _firstName,
        'lastName': _lastName,
        'phoneNumber': _phoneNumber,
        'emailAddress': _emailAddress,
        'password': _hashed_pwd,
        'type': _type,
        'name': _name,
        'address': _address
    })

    resp = jsonify({'register': True, 'user_type': _type})
    resp.status_code = 200
    return resp

@app.route('/login', methods=['POST'])
def login():
    _json = request.json
    _emailAddress = _json['emailAddress']
    _password = _json['password']

    candidate = mongo.db.candidates.find_one({'emailAddress': _emailAddress})
    recruiter = mongo.db.recruiters.find_one({'emailAddress': _emailAddress})

    if candidate and check_password_hash(candidate['password'], _password):
        access_token = create_access_token(identity=str(candidate['_id']))
        resp = jsonify({'login': True, 'user_type': 'candidate', 'access_token': access_token})
        resp.status_code = 200
        return resp
    elif recruiter and check_password_hash(recruiter['password'], _password):
        access_token = create_access_token(identity=str(recruiter['_id']))
        resp = jsonify({'login': True, 'user_type': 'recruiter', 'access_token': access_token})
        resp.status_code = 200
        return resp
    else:
        resp = jsonify({'login': False, 'message': 'Invalid credentials'})
        resp.status_code = 401
        return resp

@app.route('/update/candidate/<candidate_id>', methods=['PUT'])
def update_candidate(candidate_id):
    _json = request.json
    _firstName = _json.get('firstName')
    _lastName = _json.get('lastName')
    _phoneNumber = _json.get('phoneNumber')
    _emailAddress = _json.get('emailAddress')
    _password = _json.get('password')
    _type = _json.get('type')
    _title = _json.get('title')
    _resume_path = _json.get('resume_path')

    update_data = {}
    if _firstName:
        update_data['firstName'] = _firstName
    if _lastName:
        update_data['lastName'] = _lastName
    if _phoneNumber:
        update_data['phoneNumber'] = _phoneNumber
    if _emailAddress:
        update_data['emailAddress'] = _emailAddress
    if _password:
        _hashed_pwd = generate_password_hash(_password)
        update_data['password'] = _hashed_pwd
    if _type:
        update_data['type'] = _type
    if _title:
        update_data['title'] = _title
    if _resume_path:
        update_data['resume_path'] = _resume_path

    mongo.db.candidates.update_one({'_id': ObjectId(candidate_id)}, {'$set': update_data})

    resp = jsonify({'update': True, 'user_type': 'candidate'})
    resp.status_code = 200
    return resp

@app.route('/update/recruiter/<recruiter_id>', methods=['PUT'])
def update_recruiter(recruiter_id):
    _json = request.json
    _firstName = _json.get('firstName')
    _lastName = _json.get('lastName')
    _phoneNumber = _json.get('phoneNumber')
    _emailAddress = _json.get('emailAddress')
    _password = _json.get('password')
    _type = _json.get('type')
    _name = _json.get('name')
    _address = _json.get('address')

    update_data = {}
    if _firstName:
        update_data['firstName'] = _firstName
    if _lastName:
        update_data['lastName'] = _lastName
    if _phoneNumber:
        update_data['phoneNumber'] = _phoneNumber
    if _emailAddress:
        update_data['emailAddress'] = _emailAddress
    if _password:
        _hashed_pwd = generate_password_hash(_password)
        update_data['password'] = _hashed_pwd
    if _type:
        update_data['type'] = _type
    if _name:
        update_data['name'] = _name
    if _address:
        update_data['address'] = _address

    mongo.db.recruiters.update_one({'_id': ObjectId(recruiter_id)}, {'$set': update_data})

    resp = jsonify({'update': True, 'user_type': 'recruiter'})
    resp.status_code = 200
    return resp

#Api Offre d'emloi

@app.route('/offers/create', methods=['POST'])
def create_job_offer():
    _json = request.json
    _title = _json['title']
    _description = _json['description']
    _requirements = _json['requirements']
    _offerType = _json['offerType']
    _publicationDate = _json['publicationDate']
    _expirationDate = _json['expirationDate']
    _visible = _json['visible']
    _valide = _json['valide']

    # Créez une instance de la classe OffreEmploi
    job_offer = OffreEmploi(_title, _description, _requirements, _offerType, _publicationDate, _expirationDate, _visible, _valide)

    # Enregistrez l'offre d'emploi dans la base de données
    offer_data = {
        'title': job_offer.title,
        'description': job_offer.description,
        'requirements': job_offer.requirements,
        'offerType': job_offer.offerType,
        'publicationDate': datetime.strptime(_publicationDate, "%Y-%m-%d"),  # Convertissez la chaîne en objet datetime
        'expirationDate': datetime.strptime(_expirationDate, "%Y-%m-%d"),  # Convertissez la chaîne en objet datetime
        'visible': _visible,
        'valide': _valide
    }

    result = mongo.db.offers.insert_one(offer_data)

    # Vérifiez si l'offre d'emploi a été ajoutée avec succès
    if result.inserted_id:
        resp = jsonify({'created': True, 'job_offer_title': _title, 'offer_id': str(result.inserted_id)})
        resp.status_code = 200
    else:
        resp = jsonify({'created': False, 'error': 'Failed to create job offer'})
        resp.status_code = 500

    return resp

@app.route('/offers/update/<offer_id>', methods=['PUT'])
def update_job_offer(offer_id):
    _json = request.json
    update_data = {
        'title': _json.get('title'),
        'description': _json.get('description'),
        'requirements': _json.get('requirements'),
        'offerType': _json.get('offerType'),
        'publicationDate': datetime.strptime(_json.get('publicationDate'), "%Y-%m-%d"),
        'expirationDate': datetime.strptime(_json.get('expirationDate'), "%Y-%m-%d"),
        'visible': _json.get('visible'),
        'valide': _json.get('valide')
    }
    mongo.db.offers.update_one({'_id': ObjectId(offer_id)}, {'$set': update_data})
    resp = jsonify({"message": "Job offer updated successfully"})
    resp.status_code = 200
    return resp

@app.route('/offers')
def get_all_offers():
    all_offers = mongo.db.offers.find()
    resp = dumps(all_offers)
    return resp

@app.route('/offers/<title>')
def get_offers_by_title(title):
    offers_by_title = mongo.db.offers.find({'title': title})
    resp = dumps(offers_by_title)
    return resp

@app.route('/offers/delete/<offer_id>', methods=['DELETE'])
def delete_job_offer(offer_id):
    mongo.db.offers.delete_one({'_id': ObjectId(offer_id)})
    resp = jsonify({"message": "Job offer deleted successfully"})
    resp.status_code = 200
    return resp

if __name__ == "__main__":
    app.run(debug=True)

