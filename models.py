
# models.py

class User:
    def __init__(self, firstName, lastName, phoneNumber, emailAddress, password):
        self.firstName = firstName
        self.lastName = lastName
        self.phoneNumber = phoneNumber
        self.emailAddress = emailAddress
        self.password = password

class Candidate(User):
    def __init__(self, firstName, lastName, phoneNumber, emailAddress, password, title, resume_path):
        super().__init__(firstName, lastName, phoneNumber, emailAddress, password)
        self.title = title
        self.resume_path = resume_path

class Recruiter(User):
    def __init__(self, firstName, lastName, phoneNumber, emailAddress, password, name, address):
        super().__init__(firstName, lastName, phoneNumber, emailAddress, password)
        self.name = name
        self.address = address

class OffreEmploi:
    def __init__(self, title, description, requirements, offerType, publicationDate, expirationDate, visible, valide):
        self.title = title
        self.description = description
        self.requirements = requirements
        self.offerType = offerType
        self.publicationDate = publicationDate
        self.expirationDate = expirationDate
        self.visible = visible
        self.valide = valide

