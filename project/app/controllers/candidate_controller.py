from flask import Blueprint, request
from services.candidate_service import CandidateService
from flask_pymongo import PyMongo
from flask_jwt_extended import jwt_required

candidate_controller = Blueprint('candidate_controller', __name__)
mongo = PyMongo()  # Create a PyMongo instance

# Initialize PyMongo within your Flask app
def init_app(app):
    mongo.init_app(app)
    CandidateService.init_service(mongo)

candidate_service = CandidateService()

@candidate_controller.route('/candidates', methods=['GET'], endpoint='get_all_candidates')
@jwt_required()
def get_all_candidates():
    return candidate_service.get_all_candidates()

@candidate_controller.route('/candidates/<candidate_id>', methods=['GET'], endpoint='get_candidate_by_id')
@jwt_required()
def get_candidate_by_id(candidate_id):
    return candidate_service.get_candidate_by_id(candidate_id)

@candidate_controller.route('/candidates/register', methods=['POST'], endpoint='register_candidate')
def register_candidate():
    json_data = request.get_json()
    return candidate_service.register_candidate(json_data)

@candidate_controller.route('/candidates/<candidate_id>', methods=['PUT'], endpoint='update_candidate')
@jwt_required()
def update_candidate(candidate_id):
    json_data = request.get_json()
    return candidate_service.update_candidate(candidate_id, json_data)

@candidate_controller.route('/candidates/<candidate_id>', methods=['DELETE'], endpoint='delete_candidate')
@jwt_required()
def delete_candidate(candidate_id):
    return candidate_service.delete_candidate(candidate_id)
