from flask import Blueprint, request, jsonify
from services.job_offer_service import JobOfferService
from flask_pymongo import PyMongo
from flask_jwt_extended import jwt_required, get_jwt_identity

job_offer_controller = Blueprint('job_offer_controller', __name__)
mongo = PyMongo()  # Create a PyMongo instance

# Initialize PyMongo within your Flask app
def init_app(app):
    mongo.init_app(app)
    JobOfferService.init_service(mongo)

job_offer_service = JobOfferService()

@job_offer_controller.route('/job_offers', methods=['POST'], endpoint='create_job_offer')
@jwt_required()
def create_job_offer():
    current_user = get_jwt_identity()
    json_data = request.get_json()
    return job_offer_service.create_job_offer(json_data)

@job_offer_controller.route('/job_offers/<offer_id>', methods=['GET'], endpoint='get_offer_by_id')
@jwt_required()
def get_offer_by_id(offer_id):
    return job_offer_service.get_offer_by_id(offer_id)


@job_offer_controller.route('/job_offers/<offer_id>', methods=['PUT'], endpoint='update_job_offer')
@jwt_required()
def update_job_offer(offer_id):
    json_data = request.get_json()
    return job_offer_service.update_job_offer(offer_id, json_data)

@job_offer_controller.route('/job_offers', methods=['GET'], endpoint='get_all_offers')
@jwt_required()
def get_all_offers():
    return job_offer_service.get_all_offers()

@job_offer_controller.route('/job_offers/<offer_id>', methods=['DELETE'], endpoint='delete_job_offer')
@jwt_required()
def delete_job_offer(offer_id):
    return job_offer_service.delete_job_offer(offer_id)

@job_offer_controller.route('/job_offers/by_title/<title>', methods=['GET'], endpoint='get_offers_by_title')
@jwt_required()
def get_offers_by_title(title):
    return job_offer_service.get_offers_by_title(title)

@job_offer_controller.route('/job_offers/by_companyName/<companyName>', methods=['GET'], endpoint='get_offers_by_companyName')
@jwt_required()
def get_offers_by_companyName(companyName):
    return job_offer_service.get_offers_by_companyName(companyName)
