from flask import Blueprint, request, jsonify
from services.recruiter_service import RecruiterService
from flask_pymongo import PyMongo
from flask_jwt_extended import jwt_required, get_jwt_identity

recruiter_controller = Blueprint('recruiter_controller', __name__)
mongo = PyMongo()  # Create a PyMongo instance

# Initialize PyMongo within your Flask app
def init_app(app):
    mongo.init_app(app)
    RecruiterService.init_service(mongo)

recruiter_service = RecruiterService()

@recruiter_controller.route('/recruiters/<recruiter_id>', methods=['GET'], endpoint='get_recruiter_by_id')
@jwt_required()
def get_recruiter_by_id(recruiter_id):
    return recruiter_service.get_recruiter_by_id(recruiter_id)

@recruiter_controller.route('/recruiters', methods=['GET'], endpoint='get_all_recruiters')
@jwt_required()
def get_all_recruiters():
    return recruiter_service.get_all_recruiters()

@recruiter_controller.route('/recruiters/<recruiter_id>', methods=['DELETE'], endpoint='delete_recruiter')
@jwt_required()
def delete_recruiter(recruiter_id):
    return recruiter_service.delete_recruiter(recruiter_id)

@recruiter_controller.route('/recruiters/register', methods=['POST'], endpoint='register_recruiter')
def register_recruiter():
    json_data = request.get_json()
    return recruiter_service.register_recruiter(json_data)

@recruiter_controller.route('/recruiters/<recruiter_id>', methods=['PUT'], endpoint='update_recruiter')
@jwt_required()
def update_recruiter(recruiter_id):
    json_data = request.get_json()
    return recruiter_service.update_recruiter(recruiter_id, json_data)
