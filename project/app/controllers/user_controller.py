from flask import Blueprint, request
from services.user_service import UserService
from flask_pymongo import PyMongo
from flask_jwt_extended import jwt_required

user_controller = Blueprint('user_controller', __name__)
mongo = PyMongo()  # Create a PyMongo instance

# Initialize PyMongo within your Flask app
def init_app(app):
    mongo.init_app(app)
    UserService.init_service(mongo)

user_service = UserService()

@user_controller.route('/users', methods=['GET'], endpoint='get_all_users')
@jwt_required()
def get_all_users():
    return user_service.get_all_users()

@user_controller.route('/users/<user_type>', methods=['GET'], endpoint='get_all_users_by_type')
@jwt_required()
def get_all_users_by_type(user_type):
    return user_service.get_all_users_by_type(user_type)

@user_controller.route('/admin/register', methods=['POST'], endpoint='register_admin')
def register_admin():
    json_data = request.get_json()
    return user_service.register_admin(json_data)

@user_controller.route('/admin/<admin_id>', methods=['PUT'], endpoint='update_admin')
@jwt_required()
def update_admin(admin_id):
    json_data = request.get_json()
    return user_service.update_admin(admin_id, json_data)

@user_controller.route('/admin/<admin_id>', methods=['DELETE'], endpoint='delete_admin')
@jwt_required()
def delete_admin(admin_id):
    return user_service.delete_admin(admin_id)

@user_controller.route('/login', methods=['POST'], endpoint='login')
def login():
    json_data = request.get_json()
    return user_service.login(json_data)
