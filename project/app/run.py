from flask import Flask
from flask_cors import CORS
from flask_pymongo import PyMongo
from flask_jwt_extended import JWTManager
from controllers.user_controller import user_controller
from controllers.recruiter_controller import recruiter_controller
from controllers.job_offer_controller import job_offer_controller
from controllers.candidate_controller import candidate_controller
from services import CandidateService
from services import UserService
from services import RecruiterService
from services import JobOfferService
from datetime import timedelta

app = Flask(__name__)

# Configuration de la base de données MongoDB
app.config['MONGO_URI'] = "mongodb://localhost:27017/Recruitement"
app.config['JWT_SECRET_KEY'] = 'secret_key'
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(hours=24)
mongo = PyMongo(app)


# Activation de CORS pour permettre les requêtes depuis n'importe quel domaine
CORS(app)

jwt = JWTManager(app)

# Initialize PyMongo and all services
JobOfferService.init_service(mongo)
RecruiterService.init_service(mongo)
UserService.init_service(mongo)
CandidateService.init_service(mongo)

def init_app(app):
    app.register_blueprint(user_controller)
    app.register_blueprint(recruiter_controller)
    app.register_blueprint(job_offer_controller)
    app.register_blueprint(candidate_controller)


if __name__ == '__main__':
    # Add this line before app.run(debug=True)
    init_app(app)
    app.run(debug=True)
