# services/candidate_service.py
from bson.errors import InvalidId
from bson.json_util import dumps
from flask import jsonify
from werkzeug.security import generate_password_hash
from bson.objectid import ObjectId

class CandidateService:
    mongo = None  # Class-level variable to store the mongo instance

    @classmethod
    def init_service(cls, mongo):
        cls.mongo = mongo

    # def get_candidate_by_id(self, candidate_id):
    #     candidate = self.mongo.db.candidates.find_one({'_id': ObjectId(candidate_id)})
    #     return jsonify(candidate)

    def get_all_candidates(self):
        candidates = self.mongo.db.candidates.find()
        return jsonify(candidates)

    def delete_candidate(self, candidate_id):
        self.mongo.db.candidates.delete_one({'_id': ObjectId(candidate_id)})
        return jsonify("Candidate deleted successfully")

    def register_candidate(self, json_data):
        _firstName = json_data['firstName']
        _lastName = json_data['lastName']
        _phoneNumber = json_data['phoneNumber']
        _emailAddress = json_data['emailAddress']
        _password = json_data['password']
        _type = json_data['type']
        _title = json_data['title']
        _resume_path = json_data['resume_path']

        _hashed_pwd = generate_password_hash(_password)

        id = self.mongo.db.candidates.insert_one({
            'firstName': _firstName,
            'lastName': _lastName,
            'phoneNumber': _phoneNumber,
            'emailAddress': _emailAddress,
            'password': _hashed_pwd,
            'type': _type,
            'title': _title,
            'resume_path': _resume_path
        })

        return jsonify({'register': True, 'user_type': _type})

    def update_candidate(self, candidate_id, json_data):
        _json = json_data
        _firstName = _json.get('firstName')
        _lastName = _json.get('lastName')
        _phoneNumber = _json.get('phoneNumber')
        _emailAddress = _json.get('emailAddress')
        _password = _json.get('password')
        _type = _json.get('type')
        _title = _json.get('title')
        _resume_path = _json.get('resume_path')

        update_data = {}
        if _firstName:
            update_data['firstName'] = _firstName
        if _lastName:
            update_data['lastName'] = _lastName
        if _phoneNumber:
            update_data['phoneNumber'] = _phoneNumber
        if _emailAddress:
            update_data['emailAddress'] = _emailAddress
        if _password:
            _hashed_pwd = generate_password_hash(_password)
            update_data['password'] = _hashed_pwd
        if _type:
            update_data['type'] = _type
        if _title:
            update_data['title'] = _title
        if _resume_path:
            update_data['resume_path'] = _resume_path

        self.mongo.db.candidates.update_one({'_id': ObjectId(candidate_id)}, {'$set': update_data})

        return jsonify({'update': True, 'user_type': 'candidate'})

    def get_candidate_by_id(self, id):
        try:
            id_obj = ObjectId(id)
        except InvalidId:
            return jsonify({"error": "Invalid candidate ID format"}), 400

        try:
            candidat = self.mongo.db.candidates.find_one({'_id': id_obj})

            if candidat:
                return dumps(candidat)
            else:
                return jsonify({"error": "candidat not found"}), 404
        except Exception as e:
            return jsonify({"error": f"An error occurred: {str(e)}"}), 500
