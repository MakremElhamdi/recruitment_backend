from bson.errors import InvalidId
from bson.json_util import dumps
from bson.objectid import ObjectId
from bson import ObjectId
from models import OffreEmploi
from datetime import datetime
from flask import jsonify

class JobOfferService:
    mongo = None  # Class-level variable to store the mongo instance

    @classmethod
    def init_service(cls, mongo):
        cls.mongo = mongo

    def create_job_offer(self, json_data):
        _title = json_data['title']
        _description = json_data['description']
        _requirements = json_data['requirements']
        _offerType = json_data['offerType']
        _publicationDate = json_data['publicationDate']
        _expirationDate = json_data['expirationDate']
        _visible = json_data['visible']
        _valide = json_data['valide']
        _companyName = json_data['companyName']

        result = self.mongo.db.offers.insert_one({
            'title': _title,
            'description': _description,
            'requirements': _requirements,
            'offerType': _offerType,
            'publicationDate': _publicationDate,
            'expirationDate': _expirationDate,
            'visible': _visible,
            'valide': _valide,
            'companyName': _companyName
        })

        inserted_id = str(result.inserted_id)

        return jsonify({'created': True, 'job_offer_title': _title, 'offer_id': inserted_id}), 200

    def get_offer_by_id(self, offer_id):
        try:
            # Attempt to convert the provided offer_id to ObjectId
            offer_id_obj = ObjectId(offer_id)
        except InvalidId:
            return jsonify({"error": "Invalid offer ID format"}), 400

        try:
            # Try retrieving the job offer using ObjectId
            offer = self.mongo.db.offers.find_one({'_id': offer_id_obj})

            if offer:
                return dumps(offer)
            else:
                return jsonify({"error": "Job offer not found"}), 404
        except Exception as e:
            return jsonify({"error": f"An error occurred: {str(e)}"}), 500

    # def update_job_offer(self, offer_id, json_data):
    #     update_data = {
    #         'title': json_data.get('title'),
    #         'description': json_data.get('description'),
    #         'requirements': json_data.get('requirements'),
    #         'offerType': json_data.get('offerType'),
    #         'publicationDate': datetime.strptime(json_data.get('publicationDate'), "%Y-%m-%d"),
    #         'expirationDate': datetime.strptime(json_data.get('expirationDate'), "%Y-%m-%d"),
    #         'visible': json_data.get('visible'),
    #         'valide': json_data.get('valide')
    #     }
    #
    #     self.mongo.db.offers.update_one({'_id': ObjectId(offer_id)}, {'$set': update_data})
    #     return jsonify({"message": "Job offer updated successfully"}), 200

    # def update_job_offer(self, offer_id, json_data):
    #     update_data = {
    #         'title': json_data.get('title'),
    #         'description': json_data.get('description'),
    #         'requirements': json_data.get('requirements'),
    #         'offerType': json_data.get('offerType'),
    #     }
    #
    #     # publication_date_str = json_data.get('publicationDate')
    #     # expiration_date_str = json_data.get('expirationDate')
    #     # Example adjustment based on potential JSON structure
    #     publication_date_str = json_data.get('publicationDate', {}).get('$date')
    #     expiration_date_str = json_data.get('expirationDate', {}).get('$date')
    #
    #     print("Publication Date String:", publication_date_str)
    #     print("Expiration Date String:", expiration_date_str)
    #
    #     # Handle missing or invalid date values
    #     if publication_date_str:
    #         try:
    #             # update_data['publicationDate'] = datetime.strptime(publication_date_str, "%Y-%m-%d")
    #             update_data['publicationDate'] = datetime.strptime(publication_date_str, "%Y-%m-%d")
    #         except Exception as e:
    #             print("Error converting publication date:", e)
    #
    #     if expiration_date_str:
    #         try:
    #             # update_data['expirationDate'] = datetime.strptime(expiration_date_str, "%Y-%m-%d")
    #             update_data['expirationDate'] = datetime.strptime(expiration_date_str, "%Y-%m-%d")
    #         except Exception as e:
    #             print("Error converting expiration date:", e)
    #
    #     print("Update Data:", update_data)
    #
    #     self.mongo.db.offers.update_one({'_id': ObjectId(offer_id)}, {'$set': update_data})
    #     return jsonify({"message": "Job offer updated successfully"}), 200

    # def update_job_offer(self, offer_id, json_data):
    #     update_data = {
    #         'title': json_data.get('title'),
    #         'description': json_data.get('description'),
    #         'requirements': json_data.get('requirements'),
    #         'offerType': json_data.get('offerType'),
    #         publication_date_str = json_data.get('publicationDate'),
    #     expiration_date_str = json_data.get('expirationDate')
    #     }
    #
    #     # publication_date_str = json_data.get('publicationDate')
    #     # expiration_date_str = json_data.get('expirationDate')
    #
    #     # Example adjustment based on potential JSON structure
    #
    #
    #     print("Publication Date String:", publication_date_str)
    #     print("Expiration Date String:", expiration_date_str)
    #
    #     # Handle missing or invalid date values
    #     if publication_date_str and isinstance(publication_date_str, dict):
    #         try:
    #             update_data['publicationDate'] = datetime.strptime(publication_date_str.get('$date'),
    #                                                                "%Y-%m-%dT%H:%M:%SZ")
    #             print("publication date", update_data)
    #         except Exception as e:
    #             print("Error converting publication date:", e)
    #
    #     if expiration_date_str and isinstance(expiration_date_str, dict):
    #         try:
    #             update_data['expirationDate'] = datetime.strptime(expiration_date_str.get('$date'),
    #                                                               "%Y-%m-%dT%H:%M:%SZ")
    #             print("expiration date",update_data)
    #         except Exception as e:
    #             print("Error converting expiration date:", e)
    #
    #     print("Update Data:", update_data)
    #
    #     self.mongo.db.offers.update_one({'_id': ObjectId(offer_id)}, {'$set': update_data})
    #     return jsonify({"message": "Job offer updated successfully"}), 200

    from datetime import datetime
    from flask import jsonify
    from bson import ObjectId

    # Assurez-vous d'importer les modules nécessaires, le cas échéant

    # def update_job_offer(self, offer_id, json_data):
    #     # Initialisez le dictionnaire update_data avec les valeurs par défaut
    #     update_data = {
    #         'title': json_data.get('title'),
    #         'description': json_data.get('description'),
    #         'requirements': json_data.get('requirements'),
    #         'offerType': json_data.get('offerType'),
    #         'publicationDate': None,
    #         'expirationDate': None,
    #     }
    #
    #     # Récupérez les valeurs de date du JSON
    #     publication_date_str = json_data.get('publicationDate')
    #     expiration_date_str = json_data.get('expirationDate')
    #
    #     print("Publication Date String:", publication_date_str)
    #     print("Expiration Date String:", expiration_date_str)
    #
    #     # Gérez les valeurs de date manquantes ou invalides
    #     if publication_date_str and isinstance(publication_date_str, str):
    #         try:
    #             update_data['publicationDate'] = datetime.strptime(publication_date_str, "%Y-%m-%d")
    #             print("Publication Date:", update_data['publicationDate'])
    #         except Exception as e:
    #             print("Error converting publication date:", e)
    #
    #     if expiration_date_str and isinstance(expiration_date_str, str):
    #         try:
    #             update_data['expirationDate'] = datetime.strptime(expiration_date_str, "%Y-%m-%d")
    #             print("Expiration Date:", update_data['expirationDate'])
    #         except Exception as e:
    #             print("Error converting expiration date:", e)
    #
    #     print("Update Data:", update_data)
    #
    #     # Mettez à jour le document dans la base de données MongoDB
    #     self.mongo.db.offers.update_one({'_id': ObjectId(offer_id)}, {'$set': update_data})
    #
    #     # Retournez une réponse JSON pour indiquer que la mise à jour a réussi
    #     return jsonify({"message": "Job offer updated successfully"}), 200

    def update_job_offer(self, offer_id, json_data):
        # Initialisez le dictionnaire update_data avec les valeurs par défaut
        update_data = {
            'title': json_data.get('title'),
            'description': json_data.get('description'),
            'requirements': json_data.get('requirements'),
            'offerType': json_data.get('offerType'),
            'publicationDate': None,
            'expirationDate': None,
        }

        # Récupérez les valeurs de date du JSON
        publication_date_str = json_data.get('publicationDate')
        expiration_date_str = json_data.get('expirationDate')

        print("Publication Date String:", publication_date_str)
        print("Expiration Date String:", expiration_date_str)

        # Gérez les valeurs de date manquantes ou invalides
        if publication_date_str and isinstance(publication_date_str, str):
            try:
                update_data['publicationDate'] = publication_date_str  # Ajoutez la chaîne de caractères directement
                print("Publication Date:", update_data['publicationDate'])
            except Exception as e:
                print("Error converting publication date:", e)

        if expiration_date_str and isinstance(expiration_date_str, str):
            try:
                update_data['expirationDate'] = expiration_date_str  # Ajoutez la chaîne de caractères directement
                print("Expiration Date:", update_data['expirationDate'])
            except Exception as e:
                print("Error converting expiration date:", e)

        print("Update Data:", update_data)

        # Mettez à jour le document dans la base de données MongoDB
        self.mongo.db.offers.update_one({'_id': ObjectId(offer_id)}, {'$set': update_data})

        # Retournez une réponse JSON pour indiquer que la mise à jour a réussi
        return jsonify({"message": "Job offer updated successfully"}), 200

    def get_all_offers(self):
        all_offers = self.mongo.db.offers.find()
        return dumps(all_offers)
    def get_offers_by_title(self, title):
        offers = self.mongo.db.offers.find({'title': title})
        return dumps(offers)

    def get_offers_by_companyName(self, companyName):
        offers = self.mongo.db.offers.find({'companyName': companyName})
        return dumps(offers)

    def get_offer_by_Id(self, offer_id):
        offer = self.mongo.db.offers.find_one({'_id': ObjectId(offer_id)})
        offer['_offer_id'] = str(offer['_offer_id'])
        return jsonify(offer)
    def delete_job_offer(self, offer_id):
        self.mongo.db.offers.delete_one({'_id': ObjectId(offer_id)})
        return jsonify({"message": "Job offer deleted successfully"}), 200
