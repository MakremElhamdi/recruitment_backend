# services/recruiter_service.py
from flask import jsonify
from werkzeug.security import generate_password_hash
from bson.objectid import ObjectId
from bson.json_util import dumps

class RecruiterService:
    mongo = None  # Class-level variable to store the mongo instance

    @classmethod
    def init_service(cls, mongo):
        cls.mongo = mongo

    def get_recruiter_by_id(self, recruiter_id):
        recruiter = self.mongo.db.recruiters.find_one({'_id': ObjectId(recruiter_id)})
        # Convert ObjectId to string before jsonify
        recruiter['_id'] = str(recruiter['_id'])
        return jsonify(recruiter)



    def get_all_recruiters(self):
        recruiters = self.mongo.db.recruiters.find()
        return dumps(recruiters)

    def delete_recruiter(self, recruiter_id):
        self.mongo.db.recruiters.delete_one({'_id': ObjectId(recruiter_id)})
        return jsonify("Recruiter deleted successfully")

    def register_recruiter(self, json_data):
        _firstName = json_data['firstName']
        _lastName = json_data['lastName']
        _phoneNumber = json_data['phoneNumber']
        _emailAddress = json_data['emailAddress']
        _password = json_data['password']
        _type = json_data['type']
        _name = json_data['name']
        _address = json_data['address']

        _hashed_pwd = generate_password_hash(_password)

        id = self.mongo.db.recruiters.insert_one({
            'firstName': _firstName,
            'lastName': _lastName,
            'phoneNumber': _phoneNumber,
            'emailAddress': _emailAddress,
            'password': _hashed_pwd,
            'type': _type,
            'name': _name,
            'address': _address
        })

        return jsonify({'register': True, 'user_type': _type})

    def update_recruiter(self, recruiter_id, json_data):
        _json = json_data
        _firstName = _json.get('firstName')
        _lastName = _json.get('lastName')
        _phoneNumber = _json.get('phoneNumber')
        _emailAddress = _json.get('emailAddress')
        _password = _json.get('password')
        _type = _json.get('type')
        _name = _json.get('name')
        _address = _json.get('address')

        update_data = {}
        if _firstName:
            update_data['firstName'] = _firstName
        if _lastName:
            update_data['lastName'] = _lastName
        if _phoneNumber:
            update_data['phoneNumber'] = _phoneNumber
        if _emailAddress:
            update_data['emailAddress'] = _emailAddress
        if _password:
            _hashed_pwd = generate_password_hash(_password)
            update_data['password'] = _hashed_pwd
        if _type:
            update_data['type'] = _type
        if _name:
            update_data['name'] = _name
        if _address:
            update_data['address'] = _address

        self.mongo.db.recruiters.update_one({'_id': ObjectId(recruiter_id)}, {'$set': update_data})

        return jsonify({'update': True, 'user_type': 'recruiter'})
