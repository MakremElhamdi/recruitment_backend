# services/user_service.py
from flask import abort, jsonify, request
from werkzeug.security import generate_password_hash, check_password_hash
from flask_jwt_extended import create_access_token
from bson.json_util import dumps
from bson.objectid import ObjectId


class UserService:
    mongo = None  # Class-level variable to store the mongo instance

    @classmethod
    def init_service(cls, mongo):
        cls.mongo = mongo

    def get_all_users(self):
        candidates = self.mongo.db.candidates.find()
        recruiters = self.mongo.db.recruiters.find()
        all_users = list(candidates) + list(recruiters)
        return dumps(all_users)

    def get_all_users_by_type(self, user_type):
        valid_user_types = ['candidat', 'recruiter', 'admin']
        users = []

        if user_type.lower() not in valid_user_types:
            abort(400, {'error': 'Invalid user type'})

        if user_type == 'admin':
            # Admin can see all users, regardless of type
            users = list(self.mongo.db.candidates.find()) + list(self.mongo.db.recruiters.find())
        elif user_type == 'candidat':
            users = list(self.mongo.db.candidates.find())
        elif user_type == 'recruiter':
            users = list(self.mongo.db.recruiters.find())

        return dumps(users)

    def register_admin(self, json_data):
        _firstName = json_data['firstName']
        _lastName = json_data['lastName']
        _phoneNumber = json_data['phoneNumber']
        _emailAddress = json_data['emailAddress']
        _password = json_data['password']

        _hashed_pwd = generate_password_hash(_password)

        id = self.mongo.db.admins.insert_one({
            'firstName': _firstName,
            'lastName': _lastName,
            'phoneNumber': _phoneNumber,
            'emailAddress': _emailAddress,
            'password': _hashed_pwd,
            'type': 'admin'
        })

        return jsonify({'register': True, 'user_type': 'admin'})

    def update_admin(self, admin_id, json_data):
        _firstName = json_data.get('firstName')
        _lastName = json_data.get('lastName')
        _phoneNumber = json_data.get('phoneNumber')
        _emailAddress = json_data.get('emailAddress')
        _password = json_data.get('password')

        update_data = {}
        if _firstName:
            update_data['firstName'] = _firstName
        if _lastName:
            update_data['lastName'] = _lastName
        if _phoneNumber:
            update_data['phoneNumber'] = _phoneNumber
        if _emailAddress:
            update_data['emailAddress'] = _emailAddress
        if _password:
            _hashed_pwd = generate_password_hash(_password)
            update_data['password'] = _hashed_pwd

        self.mongo.db.admins.update_one({'_id': ObjectId(admin_id)}, {'$set': update_data})

        return jsonify({'update': True, 'user_type': 'admin'})

    def delete_admin(self, admin_id):
        self.mongo.db.admins.delete_one({'_id': ObjectId(admin_id)})
        return jsonify({"message": "Admin user deleted successfully"})

    # def login(self, json_data):
    #     _emailAddress = json_data['emailAddress']
    #     _password = json_data['password']
    #
    #     admin = self.mongo.db.admins.find_one({'emailAddress': _emailAddress})
    #     candidate = self.mongo.db.candidates.find_one({'emailAddress': _emailAddress})
    #     recruiter = self.mongo.db.recruiters.find_one({'emailAddress': _emailAddress})
    #
    #     if admin and check_password_hash(admin['password'], _password):
    #         access_token = create_access_token(identity=str(admin['_id']))
    #         return jsonify({'login': True, 'user_type': 'admin', 'access_token': access_token})
    #     elif candidate and check_password_hash(candidate['password'], _password):
    #         access_token = create_access_token(identity=str(candidate['_id']))
    #         return jsonify({'login': True, 'user_type': 'candidate', 'access_token': access_token})
    #     elif recruiter and check_password_hash(recruiter['password'], _password):
    #         access_token = create_access_token(identity=str(recruiter['_id']))
    #         return jsonify({'login': True, 'user_type': 'recruiter', 'access_token': access_token})
    #     else:
    #         return jsonify({'login': False, 'message': 'Invalid credentials'})


    def login(self, json_data):
        _emailAddress = json_data['emailAddress']
        _password = json_data['password']

        admin = self.mongo.db.admins.find_one({'emailAddress': _emailAddress})
        candidate = self.mongo.db.candidates.find_one({'emailAddress': _emailAddress})
        recruiter = self.mongo.db.recruiters.find_one({'emailAddress': _emailAddress})

        if admin and check_password_hash(admin['password'], _password):
            admin['_id'] = str(admin['_id'])  # Convert ObjectId to string for JSON serialization
            access_token = create_access_token(identity=admin['_id'])
            admin['access_token'] = access_token
            return jsonify({'login': True,  'user_data': admin})
        elif candidate and check_password_hash(candidate['password'], _password):
            candidate['_id'] = str(candidate['_id'])
            access_token = create_access_token(identity=candidate['_id'])
            candidate['access_token'] = access_token
            return jsonify({'login': True, 'user_data': candidate})
        elif recruiter and check_password_hash(recruiter['password'], _password):
            recruiter['_id'] = str(recruiter['_id'])
            access_token = create_access_token(identity=recruiter['_id'])
            recruiter['access_token'] = access_token
            return jsonify({'login': True,  'user_data': recruiter})
        else:
            return jsonify({'login': False, 'message': 'Invalid credentials'})


